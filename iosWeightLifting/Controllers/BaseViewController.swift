//
//  ViewController.swift
//  iosWeightLifting
//
//  Created by Jon Terry on 10/19/18.
//  Copyright © 2018 Jonathon Terry. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController {

    @IBAction func returnToBaseView(unwindSeque: UIStoryboardSegue){}
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    


}

